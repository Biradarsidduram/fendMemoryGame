/*
 * Create a list that holds all of your cards
 */


/*
 * Display the cards on the page
 *   - shuffle the list of cards using the provided "shuffle" method below
 *   - loop through each card and create its HTML
 *   - add each card's HTML to the page
 */

// Shuffle function from http://stackoverflow.com/a/2450976
function shuffle(array) {
    var currentIndex = 0 ,arr = [],ran;
    var spanMoves = document.querySelector('span.moves');
    spanMoves.innerText = 0;
    while (currentIndex < array.length) {
        arr.push(array[currentIndex].className);
        currentIndex +=1;
    }
    for(var i = 0;i<array.length;i++){
		ran = Math.floor(Math.random()*arr.length);
    	array[i].className = arr[ran];
    	array[i].parentNode.className = 'card';
    	arr.splice(ran,1);
     }
    return array;
}
 /*
 * set up the event listener for a card. If a card is clicked:
 *  - display the card's symbol (put this functionality in another function that you call from this one)
 *  - add the card to a *list* of "open" cards (put this functionality in another function that you call from this one)
 *  - if the list already has another card, check to see if the two cards match
 *    + if the cards do match, lock the cards in the open position (put this functionality in another function that you call from this one)
 *    + if the cards do not match, remove the cards from the list and hide the card's symbol (put this functionality in another function that you call from this one)
 *    + increment the move counter and display it on the page (put this functionality in another function that you call from this one)
 *    + if all cards have matched, display a message with the final score (put this functionality in another function that you call from this one)
 */
const deck = document.querySelector('.deck');
let res = document.querySelector('.restart');
let min = document.querySelector('span.min');
let sec = document.querySelector('span.sec');
let time,timeOn = true;
let moves = document.querySelector('.moves');
let stars = document.querySelectorAll('.stars li ');
let box = [],matched = [],temp = true;
let resAnimate = false;
//popup variables 
let overlay = document.querySelector('.overlay');
let close = document.querySelector('.overlay button');
let playAgain = document.querySelector('.play');
let spanMoves = document.querySelector('.overlay span.moves');
let starValue = document.querySelector('.overlay span.starValue');
let starCount = 0;
let timeDisplay = document.querySelector('.overlay span.time');
let starDisplay = document.querySelector('.overlay span.star');
let trophy = document.querySelector('.overlay i.fa-trophy');
let cont = document.querySelector('div.container');
res.addEventListener('click',function () {
	resAnimate = true;
	reset();	// reset the values
});

function reset(){
   //reset score,time,shuffle elements
   let items = document.querySelectorAll('li.card i');
   //time reset
   min.textContent = '00';
   sec.textContent = '00';
   if(resAnimate){
	   	res.classList.toggle('rotate'); //add rotate animation
	   	window.setTimeout(function(){
	   		 res.classList.toggle('rotate'); // remove rotate animation
	    },500);
   }
   //shuffle items
   shuffle(items);
   //moves reset
   moves.textContent = '0';
   //stars reset 
   stars.forEach(function(val) {
   		val.style.color = 'inherit';
   });
   starDisplay.innerHTML ='';
   //clear time interval and make time on true
   clearInterval(time);
   timeOn = true;
   temp = true;
   matched = [];
   box = [];
}

function timer(){
   sec.textContent++;
   sec.textContent = sec.textContent <10 ? '0'+sec.textContent : sec.textContent;
   if(sec.textContent > 59){
   	   ++min.textContent;
   	   min.textContent = min.textContent <10 ? '0'+ min.textContent : min.textContent;
   	   sec.textContent = '00';
   }
}
deck.addEventListener('click',function(evt){
    if(evt.target.tagName==='LI'){
    	//timer function only one time..
    	if (timeOn) {
    	//if the game gets completed clear Interval
	    	time = setInterval(timer,1000);
	    	timeOn = false;
    	}
     	if(!temp && box.length > 0){
		// temp = false if the same element is clicked twice..
    		temp = box[0].children[0].title!==evt.target.children[0].title ? true : false;
     	}
    	//don't accept matched elements and repeated elements and not more than 2 elements at a time..
     	if(box.length <2 && temp && matched.indexOf(evt.target.children[0].title)==-1 ){
 			box.push(evt.target);
      		evt.target.classList.add('show');
    		evt.target.classList.add('open');
    		temp = false;
    	}
    	// box[box.length-1].classList.add('show');
    	if(box.length ===2 ){
      			++moves.textContent;
	    		if(box[0].children[0].className===box[1].children[0].className){
	    			//add success animation and match to elements
	    			box.forEach(function(val){
	    				val.classList.add('success');
	    				val.classList.add('match');
	    				matched.push(val.children[0].title);
	    			});
	    			temp = true;
 	    		}
	    		else{
	    			box.forEach(function(val){
					val.classList.add('unsuccess');
 					window.setTimeout(function(){
						val.classList.remove('unsuccess');
						val.classList.remove('show');
						val.classList.remove('open');
						box.shift();
						temp = true;
						},500);
				});
	    	}
	    	box = [];
     	}
     }
     // makes star status
     if(parseInt(moves.textContent) <=10 || parseInt(moves.textContent)===11 || parseInt(moves.textContent) === 16)
     	 makeStar(parseInt(moves.textContent));
     //finish game
     if(matched.length===16)
         finish();	
});
function makeStar(stat){
	if(stat===11)
		 stars[2].style.color = "#fff";
	if(stat ===16)
	     stars[1].style.color = '#fff';
	starCount = stat <=10 ? 3 : stat>10 && stat <=15 ? 2 : 1;
}
function finish(){
	window.setTimeout(function () {
		overlay.style.display = 'block';
		cont.style.filter  = 'blur(2px)';
 	},700);
 	//close button
	close.addEventListener('click',function () {
		 cont.style.filter = 'blur(0px)';
		 overlay.style.display = 'none';
	});
	clearInterval(time);
	//display moves and star and time values to overlay;
	spanMoves.textContent = moves.textContent;
	starValue.textContent = starCount;
	timeDisplay.textContent = min.textContent+':'+sec.textContent;
	//color the trophy
	trophy.style.color = starCount===3 ? 'gold' : starCount===2 ? 'silver' : 'inherit';
	//display stars on to the overlay
	for(var i = 0 ;i <starCount;i++){
		starDisplay.innerHTML+='<li class = "fa fa-star"></li>';
		starDisplay.children[i].style.color = trophy.style.color;
	}
	playAgain.addEventListener('click',function () {
		window.setTimeout(function(){
			//call reset function
			resAnimate = false;
 		    cont.style.filter = 'blur(0px)';
 			reset();
			overlay.style.display = 'none';
 		},500);
 	});
}