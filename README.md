# Memory Game Project

## Table of Contents

* [Instructions](#instructions)
* [How To Play](#How To Play)

## Instructions

The starter project has some HTML and CSS styling to display a static version of the Memory Game project. You'll need to convert this project from a static project to an interactive one. This will require modifying the HTML and CSS files, but primarily the JavaScript file.
 
The successful completion of memory game functionality is achieved and can be tested by playing the game..

## How To Play

Click the box elements to get matched and finish the game within a given amount of time with a minimum number of moves untill all the clicked elements are matched.

Want to play the game just click below...

https://biradarsidduram.gitlab.io/fendMemoryGame
